/**
 * Cita.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  schema:true,
  autoPK: true,
  attributes: {
    id_client: {
      model: 'Cliente',
      via: 'id',
      required: true
    },
    fecha: {
      type: 'date',
      required: true
    },
    hora: {
      type: 'string'
    }
  }
};

