/**
 * ClienteController
 *
 * @description :: Server-side logic for managing clientes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    create: function(req,res) {
        Cliente.create(req.body).exec((error, data) => {
            if (error) {
                return res.status(500).json({
                    code: 500,
                    message: 'Error almacenando datos',
                    content: error
                });
            } else {
                return res.status(200).json({
                    code: 200,
                    message: 'Data almacenada exitosamente',
                    content: data
                });
            }
        })
    },

    update: function(req,res) {
        Cliente.update({id: req.params.id}, req.body).exec((error, client)  =>  {
            if (error) {
                return  res.status(500).send({
                    code:500,
                    message: 'Error al actualizar datos',
                    content: error
                })
            } else {
                return res.status(200).send({
                    code: 200,
                    message: 'Datos actualizados correctamente',
                    content: client
                })
            }
        })
    },

    find: function(req,res) {
        Cliente.find().exec((error, clients)    =>  {
            if (error) {
                return res.status(500).send({
                    code: 500,
                    message: 'Error al obtener datos',
                    content: error
                })
            } else {
                return res.status(200).send({
                    code: 200,
                    message: 'Datos obtenidos exitosamente',
                    content: clients
                })
            }
        })
    },

    findOne: function(req,res) {
        Cliente.findOne({id: req.params.id}).exec((error, client)  =>  {
            if (error) {
                return res.status(500).send({
                    code: 500,
                    message: 'Error al obtener datos',
                    content: error
                })
            } else {
                return res.status(200).send({
                    code: 200,
                    message: 'Datos obtenidos exitosamente',
                    content: client
                })
            }
        })
    }
	
};

